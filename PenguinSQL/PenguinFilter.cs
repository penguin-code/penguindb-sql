using System;
using System.Linq;
using System.Collections.Generic;

namespace PenguinSQL
{
  public struct FilterArgument {
    public string name;
    public Object value;
  }

  public enum FilterOperation {AND, OR}

  public class PenguinFilter {
    private List<FilterArgument> arguments;
    private FilterOperation operation;

    public List<FilterArgument> Arguments { get => arguments; }
    public string Operation { get => operation.ToString(); }

    public PenguinFilter(FilterOperation operation = FilterOperation.AND, List<FilterArgument> arguments = null)
    {
      this.operation = operation;
      this.arguments = (arguments != null)? arguments : new List<FilterArgument>();
    }

    public void Add(string column, Object parameter)
    {
      this.Arguments.Add(new FilterArgument(){
        name = column,
        value = parameter
      });
    }

    public string prepare()
    {
      return String.Join($" {Operation} ", (Arguments).Select((arg, i) => $"{arg.name} = ?{arg.name}{i}"));
    }
  }
}