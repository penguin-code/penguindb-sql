using System;
using System.Linq;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace PenguinSQL
{
  public class PenguinData : Dictionary<string, object>{}

  public class PenguinShell
  {
    private MySqlConnection connection;
    private InvalidOperationException error;

    public MySqlConnection Connection { get => connection; }
    public InvalidOperationException Error { get => error; }

    public PenguinShell(string host, string user, string password, string database = null){
      string connectionString = this.CreateConnectionString(host, user, password);
      this.connection = new MySqlConnection(connectionString);

      if (database != null)
      {
        this.connection.Open();
        this.connection.ChangeDatabase(database);
        this.connection.Close();
      }
    }

    public string CreateConnectionString(string host, string user, string password)
    {
      return $"Server={host}; UID={user}; password={password}";
    }

    public bool insert(string table, PenguinData data)
    {
      connection.Open();
      MySqlCommand stmt = this.Connection.CreateCommand();
      string[] columns = data.Keys.ToArray();
      string sqlColumn = prepareColumns(columns);
      string sqlParam = prepareParameters(columns);

      stmt.CommandText = insertCommand(table, sqlColumn, sqlParam);
      foreach(var item in data)
      {
        stmt.Parameters.AddWithValue($"?{item.Key}", $"{item.Value}");
      }

      try
      {
        stmt.ExecuteNonQuery();
        return true;
      }
      catch (InvalidOperationException ex)
      {
        this.error = ex;
        return false;
      }
      finally
      {
        connection.Close();
      }
    }

    public List<PenguinData> read(string table, string[] columns = null, PenguinFilter filter = null){
      connection.Open();
      MySqlCommand stmt = this.Connection.CreateCommand();
      string sqlColumn = (columns == null)?  "*" : prepareColumns(columns);
      string sqlCondition = (filter == null)? null : applyCondition(filter.prepare());

      stmt.CommandText = selectCommand(table, sqlColumn, sqlCondition);

      if(filter != null)
      {
        foreach(var (item, i) in filter.Arguments.Select((item, i) => (item, i)))
        {
          stmt.Parameters.AddWithValue($"?{item.name}{i}", item.value);
        }
      }

      MySqlDataReader rows = stmt.ExecuteReader();
      List<PenguinData> result = new List<PenguinData>();
      while(rows.Read())
      {
        PenguinData penguin = new PenguinData();
        for(int i = 0; i < rows.FieldCount; i++)
        {
          penguin[rows.GetName(i)] = rows.GetValue(i);
        }
        result.Add(penguin);
      }
      connection.Close();
      return result;
    }

    public int delete(string table, PenguinFilter filter = null)
    {
      connection.Open();
      MySqlCommand stmt = connection.CreateCommand();
      string sqlCondition = (filter == null)? null : applyCondition(filter.prepare());

      stmt.CommandText = deleteCommand(table, sqlCondition);

      if(filter != null)
      {
        foreach(var (item, i) in filter.Arguments.Select((item, i)=> (item, i)))
        {
          stmt.Parameters.AddWithValue($"?{item.name}{i}", item.value);
        }
      }

      int result = stmt.ExecuteNonQuery();
      connection.Close();   
      return result;
    }

    public int update(string table, PenguinData data, PenguinFilter filter = null)
    {
      connection.Open();
      MySqlCommand stmt = connection.CreateCommand();
      string sqlParam = prepareUpdate(data.Keys.ToArray());
      string sqlCondition = (filter == null)? null : applyCondition(filter.prepare());

      stmt.CommandText = updateCommand(table, sqlParam, sqlCondition);
      
      foreach(var item in data){
        stmt.Parameters.AddWithValue($"?{item.Key}", item.Value);
      }
      
      if(filter != null)
      {
        foreach(var (item, i) in filter.Arguments.Select((item, i) => (item, i)))
        {
          stmt.Parameters.AddWithValue($"?{item.name}{i}", item.value);
        }
      }

      var result = stmt.ExecuteNonQuery();
      connection.Close();
      return result;
    }

    public string prepareColumns(string[] data)
    {
      return String.Join(",", data);
    }

    public string prepareParameters(string[] data){
      return String.Join(",", (data).Select(key => $"?{key}"));
    }

    public string prepareUpdate(string[] data){
      return String.Join(",", (data).Select(key => $"{key}=?{key}"));
    }

    public string applyCondition(string arg)
    {
      return $"WHERE {arg}";
    }
  
    public string insertCommand(string table, string column, string parameter)
    {
      return $"INSERT INTO {table}({column}) VALUES ({parameter})";
    }

    public string selectCommand(String table, string column, string condition = null)
    {
      return $"SELECT {column} FROM {table} {condition}";
    }

    public string deleteCommand(string table, string condition = null)
    {
      return $"DELETE FROM {table} {condition}";
    }

    public string updateCommand(string table, string parameters, string condition = null)
    {
      return $"UPDATE {table} SET {parameters} {condition}";
    }
  }
}