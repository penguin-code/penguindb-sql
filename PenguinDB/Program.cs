﻿using System;
using System.Linq;
using System.Collections.Generic;
using PenguinSQL;

namespace PenguinDB
{
    class Program
    {
        static void Main(string[] args)
        {
            var shell = new PenguinShell("localhost", "root", "example", "csharp");

            
            Console.WriteLine("--------Insert--------");
            var data = new PenguinData();
            data["name"] = "Kalleby";
            data["email"] = "kalleby12@hotmail.com";
            data["password"] = "123";
            shell.insert("user", data);



            Console.WriteLine("--------Query with filter--------");
            var filter = new PenguinFilter(FilterOperation.OR);
            filter.Add("name", "joesy");
            filter.Add("id", "65");

            var result = shell.read("user", null, filter);
            foreach(var item in result){
                Console.WriteLine(item["name"]);
            }




            Console.WriteLine("--------Query without filter--------");
            result = shell.read("user");
            foreach(var item in result){
                Console.WriteLine(item["name"]);
            }


            Console.WriteLine("--------Update all--------");
            var update1 = new PenguinData();
            update1["name"] = "fernando";
            update1["email"] = "ferna@mail.com";
            shell.update("user", update1);


            Console.WriteLine("--------Update with filter--------");
            var update2 = new PenguinData();
            update2["name"] = "Kalleby";
            update2["email"] = "kalleby@mail.com";

            var filter2 = new PenguinFilter(FilterOperation.OR);
            filter2.Add("id", 11);
            filter2.Add("id", 14);
            shell.update("user", update2, filter2);



            Console.WriteLine("--------Delete with filter--------");
            var delFilter = new PenguinFilter();
            delFilter.Add("name", "Antonio");

            var del = shell.delete("user", delFilter);
            Console.WriteLine(del);




            /* Console.WriteLine("--------Delete all--------");
            var del2 = shell.delete("user");
            Console.WriteLine(del2); */
        }
    }
}

