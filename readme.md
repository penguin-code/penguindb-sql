<!-- Project logo -->

![](Docs/logo.png)

<!-- Project title -->
# **PenguinSQL**
## Simple SQL Shell for C# programming

<!-- Table of contents -->
## **Table of contents**
  * [About](#PenguinSQL)
  * [Install](#Instalation)
  * [How to use](#Using)
    * [PenguinShell](#PenguinShell)
      * [Connection](#Connection)
      * [Insert](#Inserting-values)
      * [Read](#Reading-values)
      * [Update](#Updating-values)
      * [Delete](#Deleting-rows)
    * [PenguinFilter](#PenguinFilter)
      * [Reading with filter](#Reading-with-filter)
      * [Update with filter](#Updating-with-filter)
      * [Delete with filter](#Deleting-wit-filter)
<!-- /Table of contents -->

## **Instalation**
#### PenguinSQL is very simple to install so that you can find it in [Nuget package Manager](https://www.nuget.org/packages/PenguinSQL/)

In order to install by **Package Manager** you can use the following command:
```
PM> Install-Package PenguinSQL -Version 1.2.0
```

Otherwise, you can choose run by **.NET CLI**
```
dotnet add package PenguinSQL --version 1.2.0
```

Also you can use **Package Reference** to import this package for your project:
```
<PackageReference Include="PenguinSQL" Version="1.2.0" />
```
Copy the XML node above into your project file to reference the package

## **Using**
PenguinSQL is a friendly SQL layer for your project, it means that you will be able to operate your database without writing any SQL command lines.

## **PenguinShell**
In order to be a high level layer, **PenguinSQL** have a Class responsible to convert low level SQL commands in user friendly C# functions. **PenguinShell**, at this moment, uses C# `Dictionary<key, Object>` to dinammicaly creates the SQL commands and than executes. To operate using `PenguinShell()` you can use de following methods:

  - ``insert()``: Receives data and insert it into a specific table.
  - ``read()``: Returns a data list from all or specific rows.
  - ``update()``: Query in you database updating data from all or specific rows.
  - ``delete()``: Delete all or some specific rows.

`PenguinShell()` Also support constructor parameters to open a [connection](#Connection) between your app and a specific SQL database.
  1. **host**: `string` that referecies the database host provider.
  2. **user**: `string` username to log in the database.
  3. **password**: `string` password to log in the database.
  4. **database**: `string` name of the database.

  :scream: **note** in version 1.2.0 and less you neighter choose which port you want to use nor change the database by ``PenguinShell()`` interface, so that you need to use `Connection` property to manually acess to your connection. But this issues are listed to come in the next update, so don't worry about that :blush: .


## **Connection**
To connect your application to a database you can pass the connection parameters during [`PenguinShell()`](#PenguinShell) instance. Follow the example bellow:

```
using PenguinSQL;

/*Some code*/

PenguinShell shell = new PenguinShell("localhost", "root", "123", "myDatabase");
```

## **Inserting values**
To insert values in your database you can use the `PenguinData` object to store your data an then `insert()` to save this data in your database.

`insert(string table, PenguinData data);`

```
using PenguinSQL;

PenguinShell shell = new PenguinShell("localhost", "root", "123", "myDatabase");

PenguinData data = new PenguinData();
data["name"] = "example";
data["email"] = "example@email.com";
data["password"] = "123";
shell.insert("users", data);
```

## **Reading values**
In order to read data from some table you can use `read()` method. This method will query in a specific table and than will return a `List<PenguinData>` that can be iterable using functions like `foreach()`.

`read(string table, string[] columns, PenguinFilter filter)`

```
using PenguinSQL;

PenguinShell shell = new PenguinShell("localhost", "root", "123", "myDatabase");

List<PenguinData> result = shell.read("user");
foreach(var item in result){
    Console.WriteLine(item["name"]);
}
```

## **Updating values**
If you need to update some value you can use the `update()` method that will receive a `PenguinData` object with all changes to apply in your database registry. But be carefull the `update()` method will update all registries from your table if you not specify a filter. When finish this method will returns a `int` with a number of updated rows.

`update(string table, PenguinData data, PenguinFilter filter)`

```
using PenguinSQL;

PenguinShell shell = new PenguinShell("localhost", "root", "123", "myDatabase");

PenguinData data = new PenguinData();
data["name"] = "another user";
data["email"] = "another@example.com";
shell.update("user", data);
```

## **Deleting rows**
Another usefull method is `delete()`, with this method you will able to delete rows from your table. But like the `update()` method, if you dosen't provide a filter, this will alter all rows from your table. This method also returns a `int` number of registries that had change.

`delete(string table, PenguinFilter filter)`

```
using PenguinSQL;

PenguinShell shell = new PenguinShell("localhost", "root", "123", "myDatabase");

int deleted = shell.delete("user");
Console.WriteLine(deleted);
```

## **PenguinFilter**
In order to query, update or delete specyfic rows in your table, you need to use a `PenguinFilter` Object.
With that you can select all conditions you want to provide to your queries. By default the penguin filter commes with `FilterOperation.AND` active but you can switch that to `FilterOperation.OR` in the Object constructor params. See these examples above.

## **Reading-with-filter**
```
using PenguinSQL;

PenguinShell shell = new PenguinShell("localhost", "root", "123", "myDatabase");

PenguinFilter filter = new PenguinFilter(); // Operation AND by default
filter.Add("name", "example");
filter.Add("id", "15");

// This filter will create a 'WHERE name = example AND id = 15' condition

List<PenguinData> result = shell.read("user", null, filter);
foreach(var item in result){
    Console.WriteLine(item["name"]);
}
```

## **Update with filter**
```
using PenguinSQL;

PenguinShell shell = new PenguinShell("localhost", "root", "123", "myDatabase");

PenguinData data = new PenguinData();
data["name"] = "Best user";
data["email"] = "best@example.com";

PenguinFilter filter = new PenguinFilter(FilterOperation.OR);
filter.Add("id", 11);
filter.Add("id", 14);

// This filter will generate a 'WHERE id = 11 OR id = 14' condition

shell.update("user", data, filter);
```

## **Delete with filter**
```
using PenguinSQL;

PenguinShell shell = new PenguinShell("localhost", "root", "123", "myDatabase");

PenguinFilter filter = new PenguinFilter();
filter.Add("id", 11);

int deleted = shell.delete("user", filter);
Console.WriteLine(deleted);
```